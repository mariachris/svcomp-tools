#!/usr/bin/env python3.6
import os
from subprocess import run,PIPE
import json
from random import randint
import os.path
from enum import Enum

# tools: the name of the xml files (without the suffix)
tools = [ "cbmc", "seahorn"]
toolPaths = ["/tools/cbmc","/tools/seahorn/bin"]

paths = os.environ["PATH"].split(os.pathsep)
pathsToAdd = [path for path in toolPaths if not path in paths]
os.environ["PATH"] += os.pathsep + os.pathsep.join(pathsToAdd)


outputdir = '/var/benchexec/output/'

class STATUS(Enum):
    UNKNOWN = 0
    REACHABLE = 1
    UNREACHABLE = 2

def run_benchexec(subdir):
    print("running benchexec\n")
    command = ["benchexec","--container","--read-only-dir","/","--full-access-dir",outputdir+subdir]
    if not os.path.exists(outputdir+subdir):
        run(["mkdir","-p",subdir],cwd=outputdir, stdout=PIPE)
        for tool in tools:
            command.append("/tools/"+tool+".xml")
        runres = run(command ,cwd=outputdir+subdir)
    else:
        files = [f for f in os.listdir(outputdir+subdir) if os.path.isfile(os.path.join(outputdir+subdir,f))]
        for tool in tools:
            toolFiles = [f for f in files if f.startswith(tool) and f.endswith(".txt")]
            if not toolFiles:
                command = ["benchexec","--container","--read-only-dir","/","--full-access-dir",outputdir+subdir,"/tools/"+tool+".xml"]
                run(command ,cwd=outputdir+subdir)


def getInfoForFile(filepath,filename):
    command = ["veridiff","-j",filename+".json","-i",filename+".dump.c",filepath]
    run(command ,cwd=outputdir, stdout=PIPE)
    with open(outputdir+filename+'.json') as f:
        file = json.load(f)
        return (file["numberOfReads"],file["numberOfWrites"])

def createRandomValue():
    value =""
    values="0123456789ABCDEF"
    for i in range(8):
        value += values[randint(0,15)]
    return value

def generateRandomInstrumentationFile(filename):
    command = ["generator","-j",filename+".json","initial.json"]
    run(command ,cwd=outputdir, stdout=PIPE)
    with open(outputdir+'initial.json') as f:
        return json.load(f)



def updateFileSet(fileName):
    with open("ReachSafety.set",'w') as f:
        f.write(outputdir+fileName)

def instrument(jsonData,fileName,filePath):
    with open(outputdir+"initial.json",'w') as f:
        json.dump(jsonData,f)
    instrumentedFileName = fileName +"_"+ ("read" if jsonData["isRead"] else "write") +str(jsonData["number"])+"_"+jsonData["value"]+".c"
    command = ["veridiff","-j","dummy.json","-a","initial.json","-i", instrumentedFileName,filePath]
    run(command ,cwd=outputdir, stdout=PIPE)
    return instrumentedFileName

def getStatus(resultDir):
    files = [f for f in os.listdir(resultDir) if os.path.isfile(os.path.join(resultDir,f))]
    returnValues = dict()
    for tool in tools:
        toolFiles = [f for f in files if f.startswith(tool)]
        resultFile = [f for f in toolFiles if f.endswith(".txt")][0]
        with open(os.path.join(resultDir,resultFile)) as f:
            lines = f.read().splitlines()
            if lines[-2][-1] == "1":
                returnValues[tool] = STATUS.UNKNOWN
            else:
                if lines[-3][-1] == "1" or lines[-6][-1] == "1":
                    returnValues[tool] = STATUS.REACHABLE
                else:
                    returnValues[tool] = STATUS.UNREACHABLE
    return returnValues

filename = "data_structures_set_multi_proc_false-unreach-call_ground.c"
filePath = "/var/benchexec/input/"+filename
numReads,numWrites = getInfoForFile(filePath, filename)

positionsCounter = 0
maxPositions = 10

instrumentNewPosition = True
while instrumentNewPosition:
    jsonData = generateRandomInstrumentationFile(filename)
    instrumentedFileName = instrument(jsonData,filename,filePath)
    updateFileSet(instrumentedFileName)
    run_benchexec(instrumentedFileName+".dir/")
    resultDir = outputdir+instrumentedFileName+".dir/results/"
    status = getStatus(resultDir)
    # check whether we want to go to a new position
    positionsCounter += 1
    if positionsCounter == maxPositions:
        instrumentNewPosition = False
    # check whether we want to stay at this position and just update the value
    instrumentSamePosition = True
    returnCodes = {code for tool,code in status.items()}
    if len(returnCodes) == 1:
            instrumentSamePosition = False
    while instrumentSamePosition:
        jsonData["value"]=createRandomValue()
        instrumentedFileName = instrument(jsonData,filename,filePath)
        updateFileSet(instrumentedFileName)
        run_benchexec(instrumentedFileName+".dir/")
        resultDir = outputdir+instrumentedFileName+".dir/results/"
        status = getStatus(resultDir)
        # decide whether we stay at this position
        instrumentSamePosition = False
        returnCodes = {code for tool,code in status.items()}
        if len(returnCodes) == 1:
            instrumentSamePosition = False
        else:
            instrumentSamePosition = True #TODO different situtations: all three, one of them is UNKNOWN or they contradict
        if randint(1,100) > 95:
            instrumentSamePosition = False #make sure that we do not get stuck
        